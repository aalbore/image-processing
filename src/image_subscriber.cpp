/************************************************************
* Copyright 2015 Alexandre Albore
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*   Unless required by applicable law or agreed to in writing, software
*   distributed under the License is distributed on an "AS IS" BASIS,
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*   See the License for the specific language governing permissions and
*   limitations under the License. 
*************************************************************/

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
//#include <iostream>   // std::cout
#include <string> 
#include <sstream>

#define VISUAL_OUTPUT 0

namespace enc = sensor_msgs::image_encodings;

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}



class Image_processing {


private :
    image_transport::Publisher pub_;

public:
  Image_processing(int argc, char **argv)
    {
        ros::init(argc, argv, "image_processing");

        ros::NodeHandle nh;

        image_transport::ImageTransport it(nh);
        image_transport::Subscriber sub = it.subscribe("mv_bluefox_camera_node/image_raw", 1, &Image_processing::imageCallback, this); 
        pub_ = it.advertise("camera/image_proc", 1);   
        ros::spin();
    }


    ~Image_processing()
    {
        pub_.shutdown();
    }

void publish_image(cv::Mat image)
{
    sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "rgb8", image).toImageMsg();
    ROS_INFO("[Image proc] Publishing image");
    pub_.publish(msg);
    ros::spinOnce();
}


void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    ROS_INFO("Got image!");
    cv::Mat img_rgb, channel[3];
    try
    {
        img_rgb =   cv_bridge::toCvShare(msg, "rgb8")->image;
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }

    //Invert Image 
    cv_bridge::CvImagePtr cv_ptr;
    //OpenCV expects color images to use BGR channel order.
    try
    {
            cv_ptr = cv_bridge::toCvCopy( msg, enc::RGB8); 
    }
    catch (cv_bridge::Exception& e)
    {
            //if there is an error during conversion, display it
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
    }

    //cv::Mat img_hsv;

    // cv::threshold(channel[1], channel[2], 100, 255 , cv::THRESH_BINARY);
    //cv::cvtColor(img_rgb, img_hsv, CV_RGB2HSV);

    // cv::split(img_hsv, channel);
    // cv::imshow("channel 1", channel[0]); //BH
    // cv::imshow("channel 2", channel[1]); //GS
    // cv::imshow("channel 3", channel[2]); //RV
    // cv::waitKey(3);

     int green_intensity = 0;
     int tot_pixels = img_rgb.rows*img_rgb.cols;

     for (int i = 0; i <  img_rgb.rows; ++i)
         for (int j = 0; j <  img_rgb.cols; ++j)
             green_intensity += img_rgb.at<cv::Vec3b>(i, j).val[1];

     //  Single pixel test           // green_intensity  = img_rgb.at<cv::Vec3b>(15, 15).val[1];
     //       cv::circle(img_rgb, cv::Point(15, 15), 1, cv::Scalar(0xffff), 20);

     // Writes the output on the image
     std::string bstring ="" + patch::to_string(green_intensity/(tot_pixels));
     cv::putText(img_rgb, bstring, cv::Point(img_rgb.rows/4,img_rgb.cols/4), cv::FONT_HERSHEY_COMPLEX, 2.0, CV_RGB(5,5,255), 2.0);
     if (VISUAL_OUTPUT)
     {
         cv::imshow("view", img_rgb);
         cv::waitKey(30);
     }

     // Get abundance class 
      publish_image(img_rgb);
  
}
};


int main(int argc, char **argv)
{
    Image_processing imageProcessing(argc, argv); 

    if (VISUAL_OUTPUT) {
        cv::namedWindow("view");
        cv::startWindowThread();
    }

    ros::spin();

    if (VISUAL_OUTPUT)
        cv::destroyAllWindows(); 

}
